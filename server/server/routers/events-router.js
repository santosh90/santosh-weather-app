var eventsRouter = require('express').Router();
var eventsData = require('../data/events-data');
var _ = require('lodash');

eventsRouter.post('/', function (req, res) {
    var event = req.body;
    events.push(event);
    res.status(201).json(event || {});
});
module.exports = eventsRouter;