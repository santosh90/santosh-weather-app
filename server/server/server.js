var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
///const request = require ('request');
///var eventsRouter = require('./routers/events-router');
var OAuth = require ('oauth');
var header = {
  'X-Yahoo-App-Id': 'qUD8fE5g',
};
var request = new OAuth.OAuth (
  null,
  null,
  'dj0yJmk9ek0zOVZ6TDI2TWF0JmQ9WVdrOWNWVkVPR1pGTldjbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTJl',
  '6b18c3cadb703c364710f836577a06986f26d42e',
  '1.0',
  null,
  'HMAC-SHA1',
  null,
  header
);


var app = express();
var port = 8000;
app.use(morgan('dev'));
app.use(express.static('client'));

// Enable CORS on ExpressJS to avoid cross-origin errors when calling this server using AJAX
// We are authorizing all domains to be able to manage information via AJAX (this is just for development)
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,recording-session");
    next();
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.post ('/whether', (req, res) => {
   //console.log(req.body);
  let city = req.body.city.toLowerCase();
  let country = req.body.country.toLowerCase();
   console.log(country);
   request.get (
    'https://weather-ydn-yql.media.yahoo.com/forecastrss?location='+city+','+country+'&format=json',
    null,
    null,
    function (err, data, result) {
        if (err) {
        console.log (err);
        } else {
           console.log (data);
           res.end (data);
        }
    }
    );
});


///app.use('/whether', eventsRouter);

app.listen(port);
console.log("Running app on port port. Visit: http://localhost:" + port + "/");

