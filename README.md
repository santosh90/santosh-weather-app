## Weather App 
Weather App using OpenWeatherMap api.
Initially built using React class components and rebuilt with Hooks.
For reference, class-components branch has app built using regular React class components
## To Run App
Clone repo with `git clone https://gitlab.com/santosh90/santosh-weather-app.git`
`cd` into cloned repo
run `npm install`
run `npm start`
## To Run Server 
`cd` server
  run `npm install`
  run `npm start`