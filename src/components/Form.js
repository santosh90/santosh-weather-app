import React, {useState} from 'react';

//function component
function Form({fetchWeather}) {
  
  const [city, setCity] = useState('');
  const [country, setCountry] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    fetchWeather(e, city, country);
    
  }
  
  return (
    <form onSubmit={e => handleSubmit(e)}>
         <div className="row">
          <div className="col-4">
            <label for="cars">Choose a city:</label>
          <select required="required" name="city" id="city" onChange={(e) => {setCity(e.target.value)}}>
              <option value="">--Select City--</option>;
              <option value="sunnyvale">Sunnyvale</option>
              <option value="alameda">Alameda</option>
              <option value="albany">Albany</option>
              <option value="anderson">Anderson</option>
            </select>
          </div>
          <div className="col-4">
            <label for="cars">Country:</label>
            <select name="country" id="country" onChange={(e) => {setCountry(e.target.value)}}>
              <option value="us">US</option>
            </select>
          </div>
          <div className="col-4">
            <br />
            <button>Get Weather</button>
          </div>
        </div>
    </form>
  );
}

export default Form;