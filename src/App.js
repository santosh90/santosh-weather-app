import React, { useState } from 'react';
import Titles from './components/titles';
import Form from './components/Form';
import Weather from './components/Weather';
import {useSelector, useDispatch} from 'react-redux';
import axios from 'axios';
const API_KEY = '8117032b43375b398485e40333d6f5ae';

//function component
function App(props) {
  const [city, setCity] = useState(undefined);
  const [country, setCountry] = useState(undefined);
  const [ error, setError ] = useState(undefined);
  const [ temp, setTemp ] = useState(undefined);
  const [ humidity, setHumidity ] = useState(undefined);
  const [ desc, setDesc ] = useState(undefined);
  ///const todoId=props.item.id
  const fetchWeather = async (e, cityName, countryName) => {
    try {
      ///alert(cityName);
      e.preventDefault();
      axios.post ('http://localhost:8000/whether', {
          city: cityName,
          country: 'us'}).then (function (response) {
          const data=response.data;
          console.log (response.data);
            setCity(data.location.city);
            setCountry(data.location.country);
            setTemp(data.current_observation.condition.temperature);
            setHumidity(data.current_observation.atmosphere.humidity);
            setDesc(data.current_observation.condition.text);
            setError('');
        }).catch (function (error) {
          console.log(error);
        });
        
    } catch(err) {
      console.log(err);
      setError('Please enter city and country');
      setCity('');
      setCountry('');
      setTemp('');
      setHumidity('');
      setDesc('');
    }

  }

  return (
    <div>
      <div className="wrapper">
        <div className="main">
          <div className="container">
            <div className="row">
              <div className="col-xs-5 title-container">
                <Titles />
              </div>
              <div className="col-xs-7 form-container">
                {/*pass getWeather as a prop to Form*/}
                <Form fetchWeather={fetchWeather}/>
                {/*pass states as props to Weather component to display results*/}
                <Weather
                  temp={temp}
                  city={city}
                  country={country}
                  humidity={humidity}
                  desc={desc}
                  error={error}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
